package GateGame;

class Gate implements Comparable<Gate>{
    int type;
    int level;
    boolean opened;
    Gate (int type, int level, boolean opened){
        this.type = type;
        this.level = level;
        this.opened = opened;
    }

    public int compareTo(Gate o) {
        int compareLevel = ((Gate) o).level;
        return this.level - compareLevel;
    }
}
