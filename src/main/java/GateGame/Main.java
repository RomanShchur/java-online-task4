package GateGame;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        int userGate = 0;
        int gateNumber = 10;
        Player player = new Player();
        Gate[] possibleGates = new Gate[gateNumber];
        generateGates(possibleGates);
        showRules();
        playingGame(player, userGate, gateNumber, possibleGates);
    }
    static void playingGame(Player player, int userGate, int gateNumber, Gate[] possibleGates) throws InterruptedException {
        int iteration = 0;
        int heroDies = 0;
        checkDies(iteration, heroDies, player, possibleGates);
        System.out.println("Statistically hero will die in " + heroDies + " rooms.");
        checkWin (possibleGates);
        debugGates (possibleGates);
        openGate (player, userGate, gateNumber, possibleGates);
    }
    static Gate[] generateGates(Gate[] possibleGates){
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            int a = random.nextInt(2);
            int b = random.nextInt(100);
            Gate gate1 = new Gate(a, b, false);
            possibleGates[i] = gate1;
        }
        return possibleGates;
    }
    static Player openGate(Player player, int userGate, int gateNumber, Gate[] possibleGates) throws InterruptedException {
         userGate = selectedGate(userGate, gateNumber, possibleGates);
         possibleGates[userGate].opened = true;
         int type1 = possibleGates[userGate].type;
         int level1 = possibleGates[userGate].level;
         if (type1 == 1){
             System.out.println("You found an artifact with level " + level1);
             player.level += level1;
             System.out.println("Your level increased to level " + player.level);
             playingGame(player, userGate, gateNumber, possibleGates);
         }
         else  if (type1 == 0){
             fight(level1, player, userGate, gateNumber, possibleGates);
         }
         return player;
    }
    static void fight(int level1, Player player, int userGate, int gateNumber, Gate[] possibleGates) throws InterruptedException {
        System.out.println("Your's level " + player.level);
        System.out.println("Monster's level " + level1);
        if (level1 > player.level){
            System.out.println("You lost");
            TimeUnit.SECONDS.sleep(2);
            playNew();
        }
        else if (level1 < player.level){
            System.out.println("You Won");
            playingGame(player, userGate, gateNumber, possibleGates);
        }
    }
    static void playNew() throws InterruptedException {
        System.out.println("Want to play one more time?");
        Scanner inp = new Scanner(System.in);
        System.out.println("0 = Yes, 1 = No");
        int userChose = inp.nextInt();
        switch (userChose) {
            case 0: {
                String[] arg = {"", ""};
                main(arg);
            }
            case 1: {
                System.out.println("See ya!");
                System.exit(0);
            }
            default: {
                System.err.println("Wrong input!");
                playNew();
            }
        }
    }
    static void showRules(){
        System.out.println("Hello, knight!");
        System.out.println("You've got into the dungeon, you are in round room, and you have 10 gates in front of you.");
        System.out.println("Behind the gates there are may be monster or artifact.");
        System.out.println("Artifacts can make you more skillful, while monsters are dangerous");
    }
    static int selectedGate(int userGate, int gateNumber, Gate[] possibleGates){
        Scanner inp = new Scanner(System.in);
        System.out.print("Select your gate: ");
        userGate = inp.nextInt();
        System.out.println();
        if (userGate < 0 || userGate > possibleGates.length - 1){
            System.err.println("Gate out of range!");
            selectedGate(userGate, gateNumber, possibleGates);
        }
        if (possibleGates[userGate].opened){
            System.err.println("Gate is opened yet!");
            selectedGate(userGate, gateNumber, possibleGates);
        }
        return userGate;
    }
    static  void checkWin(Gate[] possibleGates) throws InterruptedException {
        int gatesOpened = 0;
        for (Gate possibleGate : possibleGates) {
            if (possibleGate.opened) {
                gatesOpened = gatesOpened + 1;
            }
        }
        System.out.println("Gates is opened: " + gatesOpened);
        if (gatesOpened == possibleGates.length - 1){
            System.out.println("You cleaned the dungeon from monsters.");
            System.out.println("You win!");
            TimeUnit.SECONDS.sleep(2);
            playNew();
        }
    }
    static void debugGates(Gate[] possibleGates){
        System.out.println("1 is Artifact, 0 is Monster");
        for (Gate possibleGate : possibleGates) {
            System.out.print(possibleGate.level + " ");
            System.out.print(possibleGate.type + "  ");
        }
        System.out.println();
    }
    static int checkDies(int iteration, int heroDies, Player player, Gate[] possibleGates){
        if (possibleGates[iteration].type == 0 && possibleGates[iteration].level > player.level && !possibleGates[iteration].opened){
            ++heroDies;
            System.out.print(heroDies);
        }
        if (iteration < possibleGates.length - 1) {
            //checkDies(++iteration, heroDies, player, possibleGates);
            return checkDies(++iteration, heroDies, player, possibleGates);
        }
        return heroDies;
    }
    static void  sortArray(Gate[] possibleGates){
        Arrays.sort(possibleGates);
    }
}
