package ArrayTransaction;

import java.util.Scanner;

public class ArrayTransaction {
    public static void main(String[] args){
        int length1 = 10;
        int[] array1 = new int[length1];
        int[] array2 = new int[length1];
        System.out.print("Array #1 generated:");
        for (int i = 0; i < length1; i++) {
            array1[i] = ((int) (Math.random() * 51));
            System.out.print(array1[i]+" ");
        }
        System.out.print("Array #2 generated:");
        for (int i = 0; i < length1; i++) {
            array2[i] = ((int) (Math.random() * 51));
            System.out.print(array2[i]+" ");
        }
        System.out.println();
        userInput(array1, array2);
    }
    static void userInput(int[] array1, int[] array2){
        Scanner inp = new Scanner(System.in);
        System.out.println("Input option number");
        System.out.println("1\t Only elements in both arrays");
        System.out.println("2\t Only elements in single array");
        int num = inp.nextInt();
        switch (num){
            case 1:{
                bothToArr(array1, array2);
                break;
            }
            case 2:{
                userInput2(array1, array2);
                break;
            }
            default:
                System.out.println("Selection out of range.");
        }
    }
    static void userInput2(int[] array1, int[] array2){
        Scanner inp = new Scanner(System.in);
        System.out.println("Input option number");
        System.out.println("1\t Only elements in first array");
        System.out.println("2\t Only elements in second array");
        int num = inp.nextInt();
        switch (num){
            case 1:{
                oneToArr(array1);
                break;
            }
            case 2:{
                oneToArr(array2);
                break;
            }
            default:
                System.out.println("Selection out of range.");
        }
    }
    static void bothToArr(int[] array1, int[] array2){
        int[] resArray = new int[10];
        for (int i = 0; i < array1.length; i++){
            for (int j = 0; j < array2.length; j++){
                if (array1[i] == array2[i]){
                    resArray[i] = array1[i]; 
                }
            }
        }
        for (int i = 0; i < array1.length; i++){
            System.out.print(resArray[i]+" ");
        }
    }
    static void oneToArr(int[] arr){
        int[] resArray = new int[10];
        for (int i = 0; i < resArray.length; i++){
            resArray[i] = arr[i];
            System.out.print(resArray[i]+" ");
        }
    }
}
