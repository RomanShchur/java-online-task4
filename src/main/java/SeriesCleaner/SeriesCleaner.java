package SeriesCleaner;

public class SeriesCleaner {
    public static void main(String[] args) {
        int length1 = 10;
        int[] array1 = new int[length1];
        ArrayGenerator(length1, array1);
        ArrayCleaner(length1, array1);
        System.out.println("Cleaned array:");
        for (int i = 0; i < length1; i++) {
            System.out.print(array1[i]+" ");
        }
    }
    static int[] ArrayGenerator(int length1, int[] array1){
        System.out.println("Arrays generated:");
        for (int i = 0; i < length1; i++) {
            array1[i] = ((int) (Math.random() * 26));
            System.out.print(array1[i]+" ");
        }
        System.out.println();
        return array1;
    }
    static int[] ArrayCleaner(int length1, int[] array1){
        int j = 1;
        for (int i = 0; i < length1; i++) {
            while (array1[j] == array1[j + 1]) {
                array1[j] = 0;
            }
            j = i;
        }
        return array1;
    }

}
