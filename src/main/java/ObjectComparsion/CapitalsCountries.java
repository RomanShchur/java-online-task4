package ObjectComparsion;

public class CapitalsCountries implements Comparable<CapitalsCountries>{
    String country;
    String capital;
    CapitalsCountries (String inCountry, String inCapital){
        country = inCountry;
        capital = inCapital;
    }

    public String getCountry() {
        return country;
    }
    public String getCapital() {
        return capital;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public void setCapital(String capital) {
        this.capital = capital;
    }
    public int compareTo(CapitalsCountries cc) {
        return this.getCapital().compareTo(cc.getCapital());
    }
    @Override
    public String toString() {
        return ("Capital: " + this.getCapital() +
                ", Country: " + this.getCountry() + ";");
    }
}
