package ObjectComparsion;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        int index = 0;
        ArrayList<CountriesCapitals> countryList = new ArrayList<CountriesCapitals>();
        ArrayList<CapitalsCountries> capitalList = new ArrayList<CapitalsCountries>();
        fillCountryList(countryList);
        fillCapitalList(capitalList);
        System.out.println("========== Countries =============");
        for (CountriesCapitals thisc : countryList) {
            System.out.println(thisc);
        }
        System.out.println("_________ Sorted ___________");
        Collections.sort(countryList);
        for (CountriesCapitals thisc : countryList) {
            System.out.println(thisc);
        }
        System.out.println("============ Capitals ===========");

        for (CapitalsCountries thiscc : capitalList) {
            System.out.println(thiscc);
        }
        System.out.println("_________ Sorted ___________");
        Collections.sort(capitalList);
        for (CapitalsCountries thiscc : capitalList) {
            System.out.println(thiscc);
        }
        System.out.println("Searching");
        searchineElement(countryList, index);
    }
    static ArrayList<CountriesCapitals> fillCountryList (ArrayList<CountriesCapitals> countryList){
        countryList.add(new CountriesCapitals("Poland", "Warshaw"));
        countryList.add(new CountriesCapitals("USA", "Washington"));
        countryList.add(new CountriesCapitals("Ukraine", "Kiyiv"));
        countryList.add(new CountriesCapitals("Japan", "Tokyo"));
        countryList.add(new CountriesCapitals("Commonwealth of Australia", "Canberra"));
        return countryList;
    }
    static ArrayList<CapitalsCountries> fillCapitalList (ArrayList<CapitalsCountries> capitalList){
        capitalList.add(new CapitalsCountries("Poland", "Warshaw"));
        capitalList.add(new CapitalsCountries("USA", "Washington"));
        capitalList.add(new CapitalsCountries("Ukraine", "Kiyiv"));
        capitalList.add(new CapitalsCountries("Japan", "Tokyo"));
        capitalList.add(new CapitalsCountries("Commonwealth of Australia", "Canberra"));
        return capitalList;
    }
    static int searchineElement(ArrayList<CountriesCapitals> countryList, int index){
        System.out.println("Input country to search index (First arrayList)");
        Scanner sc = new Scanner(System.in);
        String searchCountry = sc.next();
        searchCountry = searchCountry.trim();
        sc.close();
        index = Collections.binarySearch(countryList, new CountriesCapitals(searchCountry, null));
        System.out.println(index);
        return index;
    }
}
