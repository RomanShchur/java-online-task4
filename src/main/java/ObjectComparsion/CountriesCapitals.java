package ObjectComparsion;

class CountriesCapitals implements Comparable<CountriesCapitals>{
    String country;
    String capital;
    CountriesCapitals (String inCountry, String inCapital){
        country = inCountry;
        capital = inCapital;
    }

    public String getCountry() {
        return country;
    }
    public String getCapital() {
        return capital;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public void setCapital(String capital) {
        this.capital = capital;
    }

    public int compareTo(CountriesCapitals cc) {
         return this.getCountry().compareTo(cc.getCountry());
     }
    @Override
    public String toString() {
        return ("Country: " + this.getCountry()+
                ", Capital: " + this.getCapital() + ";");
    }
}
