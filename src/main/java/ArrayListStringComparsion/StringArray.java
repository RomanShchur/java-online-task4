package ArrayListStringComparsion;

import java.util.Scanner;

public class StringArray {
    private int deltaSize = 0;
    private String[] sArray1 = new String[0];
    public void addString(String addstr){
        increaseArrSize(sArray1.length + deltaSize);
        sArray1[sArray1.length-1] = addstr;
    }
    void increaseArrSize(int newSize){
        if(sArray1.length > 0){
            String[] newSizeArr = new String[(newSize)];
            System.arraycopy(sArray1, 0, newSizeArr, 0, sArray1.length);
            sArray1 = new String[(newSize+1)];
            System.arraycopy(newSizeArr,0,sArray1,0,newSizeArr.length);
            newSizeArr = null;
        }
        else{
            sArray1 = new String[1];
        }
    }
    void arrPrint(int repeatCount){
        for (int i = 0; i < repeatCount; i++) {
            System.out.print(sArray1[i] + " ");
        }
    }
}