package ArrayListStringComparsion;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner inp = new Scanner(System.in);
        System.out.println("Input count of arrays: ");
        int addRepeatCount = inp.nextInt();

        long startTime = 0, endTime = 0, delta = 0;
        arrayListTime(startTime, endTime, delta, addRepeatCount);
        customArrayTime(startTime, endTime, delta, addRepeatCount);
    }
    static void arrayListTime(long startTime, long endTime, long delta, int addRepeatCount){
        System.out.println("___________________ ArrayList _________________");
        startTime = System.nanoTime();
        ArrayList<String> alist = new ArrayList<String>();
        for (int i = 0; i < addRepeatCount; i++){
            alist.add("String" + i);
        }
        for (int i = 0; i < addRepeatCount; i++) {
            System.out.print(alist.get(i) + " ");
        }
        System.out.println();
        endTime = System.nanoTime();
        delta = endTime - startTime;
        System.out.println("ArrayList time in nanoseconds = " + delta);
    }
    static void customArrayTime(long startTime, long endTime, long delta, int addRepeatCount){
        System.out.println("___________________ Custom grow-able Array _________________");
        startTime = System.nanoTime();
        StringArray sarr = new StringArray();
        for (int i = 0; i < addRepeatCount; i++){
            sarr.addString("String" + i);
        }
        sarr.arrPrint(addRepeatCount);
        System.out.println();
        endTime = System.nanoTime();
        delta = endTime - startTime;
        System.out.println("Custom StringArray time in nanoseconds = " + delta);
    }
}