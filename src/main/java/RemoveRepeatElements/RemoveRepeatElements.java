/*
 * Can be solved using custom class ArrayUtils.java
 * ArrayUtils.remove(array1, i);  at line 38.
 * Without using custom libraries this is most suitable solution
 */

package RemoveRepeatElements;

public class RemoveRepeatElements {
    public static void main(String[] args){
        int length1 = 10;
        int[] array1 = new int[length1];
        ArrayGenerator(length1, array1);
        ArrayCleaner(length1, array1);
        System.out.println("Result array:");
        for (int i = 0; i < length1; i++) {
            System.out.print(array1[i]+" ");
        }
    }
    static int[] ArrayGenerator(int length1, int[] array1){
        System.out.println("Arrays generated:");
        for (int i = 0; i < length1; i++) {
            array1[i] = ((int) (Math.random() * 26));
            System.out.print(array1[i]+" ");
        }
        System.out.println();
        return array1;
    }
    static int[] ArrayCleaner(int length1, int[] array1){
        for (int i = 0; i < length1; i++) {
            int count = 0;
            for (int j = 0; j < length1; j++) {
                if (array1[i] == array1[j]){
                    count++;
                }
            }
            if (count >= 2) {
                array1[i] = 0;
            }
        }
        return array1;
    }
}
